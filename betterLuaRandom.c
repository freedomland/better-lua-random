/*
 * betterLuaRandom.so (c) 2019 Freedom Land Team, GPL v3.0
 * 
 *  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * */

#include <unistd.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <linux/random.h>
#include "luajit-2.0/lua.h"
#include "luajit-2.0/lualib.h"
#include "luajit-2.0/lauxlib.h"

int generateRandom(const int min, const int max)
{
	int s;
	syscall(SYS_getrandom, &s, sizeof(int), 0);
	srand(s);
	
	return min + rand() % (max + 1 - min);
}

static int getRandNum(lua_State *L)
{
	const int min = luaL_checknumber(L, 1);
	const int max = luaL_checknumber(L, 2);
	const int r = generateRandom(min, max);
	
	lua_pushnumber(L, r);
	
	return 1;
}

static int getRandValue(lua_State *L)
{
	const int num = generateRandom(1, luaL_getn(L, -1));
	lua_pushinteger(L, num);
	lua_gettable(L, 1);
	
	if (lua_isnumber(L, -1))
	{
		const int r = (int)lua_tonumber(L, -1);
		lua_pushnumber(L, r);
	}
	else
	{
		const char *r = lua_tostring(L, -1);
		lua_pushstring(L, r);
	}
	
	return 1;
}

int luaopen_betterLuaRandom(lua_State *L)
{
	lua_register(L, "getRandNum", getRandNum);
	lua_register(L, "getRandValue", getRandValue);
	return 0;
}
