# Better Lua Random

C library to get ether random number or random value from table (both integer or string).

Will only works on Linux!

### What? Why?

This library will return random number from min to max or random value from table. Why? Well, less code in lua. And this is my second hello world with C Lua API.
 
### How to compile

```bash
gcc betterLuaRandom.c -lluajit-5.1 -shared -fPIC -o betterLuaRandom.so
```

Put betterLuaRandom.so in CoreScripts/lib
 
### How to use

You need include library with require("betterLuaRandom"), then you can use it like this:

```lua
print(getRandNum(1, 500)) -- will print random number from 1 to 500
	
local t = {1, 2, 3, "test"}
print(getRandValue(t)) -- will print random value from table, ether 1, 2, 3 or "test"
```

### License
betterLuaRandom.so (c) 2019 Freedom Land Team, GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
